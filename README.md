# ASP.Net Chat with WebSocket and PostgreSQL

## Цель: данный проект представляет из себя простой чат на базе ASP.Net Core MVC приложения с использованием WebSocket, а также PostgreSQL


В моделях есть сообщение Message, которое представляет из себя объект с полями Id, User и UsersMessage:

```
    public int Id { get; set; }
    public string? User { get; set; } 
    public string? UsersMessage { get; set; }
```

Далее, в контроллере представлен объект ApplicationContext для работы с БД:

```
    public DbSet<Message> Messages { get; set; } = null!;
    public ApplicationContext() => Database.EnsureCreated();
```
Который наследуется от класса DbContext и подключается к PostgreSQL в переопределенном методе OnConfiguring:

```
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=PostgreSQL15;User Id=postgres;Password=admin;");
    }
```

WebSocket-соединение реализуется с помощью Middleware. Его конструктор:
```
    public ChatWebSocketMiddleware(RequestDelegate next)
    {
        _next = next;
    }
```

Далее его мы подключаем к проекту в Program.cs:

```
    app.UseWebSockets();
    app.UseMiddleware<ChatWebSocketMiddleware>();

```

У данного класса присутствуют три метода: **Invoke**, **SendStringAsync** и **ReceiveStringAsync**


1. Метод Invoke позволяет проверять, установлено ли соединение, получает CancellationToken и пробует запустить соединение:

` _sockets.TryAdd(socketId, currentSocket);`

Далее мы попадаем в цикл, в котором до тех пор, пока не оборвалось соединение, мы получаем от метода ReceiveStringAsync текст, отправленный пользователем и если он не пустой, отправляем всем участникам WebSocket это сообщение с помощью SendStringAsync

2. Метод ReceiveStringAsync возвращает переменную типа string из потока, переданного пользователем в WebSocket. Его вызывает Invoke.


3. Метод SendStringAsync отправляет сообщение пользователя всем слушателям WebSocket. Его вызывает Invoke

В представлении присутствуют страницы: Index, InsertUserName и Privacy

В Index попадают пользователи, которые ввели имя пользователя и стали слушателями WebSocket, InsertUserName позволяет пользователям ввести свое имя для начала общения
