﻿using Microsoft.EntityFrameworkCore;

namespace TableAgregation.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Message> Messages { get; set; } = null!;
        public ApplicationContext()
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=PostgreSQL15;User Id=postgres;Password=admin;");
        }

        public async Task SaveMessage(Message UserMessage)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                // Добавление
                await db.Messages.AddAsync(UserMessage);
                await db.SaveChangesAsync();
            }
        }

        public async Task<List<Message>> ReturnAllMessage()
        {
            using (ApplicationContext db = new ApplicationContext())
            {

                // получаем объекты из бд и выводим на консоль
                var messages = await db.Messages.ToListAsync();
                return messages;
            }
        }
    }
}
