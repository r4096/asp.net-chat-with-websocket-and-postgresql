﻿using ChatRoom.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TableAgregation.Models;

namespace ChatRoom.Controllers
{
    public class HomeController : Controller
    {
        public ApplicationContext db;

        [HttpGet]
        public IActionResult Index()
        {
            return View("InsertUserName");
        }

        [HttpPost]
        public IActionResult Index(string username)
        {
            return View("Index", username);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}