﻿using Microsoft.EntityFrameworkCore;

namespace TableAgregation.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string? User { get; set; } 
        public string? UsersMessage { get; set; }
    }

}

